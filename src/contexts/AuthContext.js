import React, { createContext, useContext, useState, useEffect } from "react";

import { auth, db } from "../firebase-config.js";
import { signInWithPopup, GoogleAuthProvider, signOut } from "firebase/auth";
import {
  collection,
  getDoc,
  getDocs,
  setDoc,
  deleteDoc,
  doc,
  where,
  query,
} from "firebase/firestore";

const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

function AuthProvider({ children }) {
  const [isProcessing, setIsProcessing] = useState(false);
  const [currentUser, setCurrentUser] = useState({});
  const [toastList, setToastList] = useState([]);

  const signInWithGoogle = () => {
    const provider = new GoogleAuthProvider();
    signInWithPopup(auth, provider)
      .then((result) => {
        setCurrentUser(result.user);
        setToast({
          type: "success",
          title: "Login successful",
          description: `Hi ${result.user.displayName}! Welcome to TheMovie` ,
          autoDeleteTime: 5000,
        });
      })
      .catch((error) => {
        setToast({
          type: "danger",
          title: "Error",
          description: error.message,
          autoDeleteTime: 5000,
        });
      });
  };

  const signOutInfo = () => {
    signOut(auth)
      .then(() => {
        setToast({
          type: "success",
          title: "Success",
          description: "Sign out successful",
          autoDeleteTime: 5000,
        });
      })
      .catch(() => {
        setToast({
          type: "danger",
          title: "Error",
          description: "An error happened.",
          autoDeleteTime: 5000,
        });
      });
  };

  const getListFavoriteMovies = async () => {
    if (!hasAuth()) {
      return;
    }
    const q = query(
      collection(db, "FAVORITE_MOVIES"),
      where("user_id", "==", currentUser.uid || "")
    );
    const querySnapshot = await getDocs(q);
    let res = [];
    querySnapshot.forEach((doc) => {
      res.push(doc.data());
    });
    return res;
  };

  const getFavoriteMovie = async (movieId) => {
    if (!hasAuth()) {
      return;
    }
    const ref = doc(
      db,
      "FAVORITE_MOVIES",
      currentUser.uid + movieId.toString()
    );
    const docSnap = await getDoc(ref);
    if (docSnap.exists()) {
      return docSnap.data();
    }
  };

  const createFavoriteMovie = async (movie) => {
    setIsProcessing(true);
    const { id, poster_path, title, original_name, seasons } = movie;
    const ref = doc(db, "FAVORITE_MOVIES", currentUser.uid + id);
    const docSnap = await getDoc(ref);
    if (docSnap.exists()) {
      setToast({
        type: "warning",
        title: "Warning",
        description: "Movie already exists in favorites",
        autoDeleteTime: 5000,
      });
      setIsProcessing(false);
      return;
    }
    try {
      await setDoc(ref, {
        movie_id: id,
        poster_path: poster_path,
        title: title || original_name,
        user_id: currentUser.uid,
        type: seasons ? "tv" : "movie",
      });
    } catch (e) {
      setToast({
        type: "danger",
        title: "Error",
        description: e.message,
        autoDeleteTime: 5000,
      });
    }
    setIsProcessing(false);
  };

  const deleteFavoriteMovie = async (movieId) => {
    setIsProcessing(true);
    const ref = doc(db, "FAVORITE_MOVIES", currentUser.uid + movieId);
    const docSnap = await getDoc(ref);
    if (!docSnap.exists()) {
      setToast({
        type: "warning",
        title: "Warning",
        description: "Movie does not exist",
        autoDeleteTime: 5000,
      });
      setIsProcessing(false);
      return;
    }
    await deleteDoc(ref)
      // .then(() => {
      //   setToast({
      //     type: "success",
      //     title: "Success",
      //     description: "Movie deleted successfully",
      //     autoDeleteTime: 5000,
      //   });
      // })
      .catch((e) => {
        setToast({
          type: "danger",
          title: "Error",
          description: e.message,
          autoDeleteTime: 5000,
        });
      });
    setIsProcessing(false);
  };

  const toggleFavoriteMovie = async (movie) => {
    if (!hasAuth()) {
      setToast({
        type: "warning",
        title: "Warning",
        description: "Login to use this feature",
        autoDeleteTime: 5000,
      });
      return;
    }
    isFavoriteMovie(movie.id).then((data) => {
      if (!data) {
        return createFavoriteMovie(movie);
      }
      return deleteFavoriteMovie(movie.id);
    });
  };

  const isFavoriteMovie = async (movie_id) => {
    const ref = doc(
      db,
      "FAVORITE_MOVIES",
      currentUser.uid + movie_id.toString()
    );
    const docSnap = await getDoc(ref);
    if (docSnap.exists()) {
      return true;
    }
    return false;
  };

  const hasAuth = () => {
    if (currentUser) {
      return true;
    }
    return false;
  };

  function setToast(toast){
    setToastList([...toastList, toast])
  }

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      setCurrentUser(user);
    });
  }, []);

  const value = {
    currentUser,
    isProcessing,
    setIsProcessing,
    signInWithGoogle,
    signOutInfo,
    getFavoriteMovie,
    createFavoriteMovie,
    deleteFavoriteMovie,
    toggleFavoriteMovie,
    getListFavoriteMovies,
    toastList,
    setToast,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export default AuthProvider;
