import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBqg1-NbkoI0ziLj4V5M_dVHbh263kS5t0",
  authDomain: "the-movie-16cf5.firebaseapp.com",
  databaseURL: "https://the-movie-16cf5-default-rtdb.firebaseio.com",
  projectId: "the-movie-16cf5",
  storageBucket: "the-movie-16cf5.appspot.com",
  messagingSenderId: "385475795352",
  appId: "1:385475795352:web:7a0371b8f6044c993373ed",
  measurementId: "G-RSM134909B"
};

// eslint-disable-next-line no-unused-vars
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const db = getFirestore(app);