import React, { memo, useState, useEffect, useCallback, useRef } from "react";
import PropTypes from "prop-types";

import checkIcon from "../../assets/images/check.svg";
import errorIcon from "../../assets/images/error.svg";
import infoIcon from "../../assets/images/info.svg";
import warningIcon from "../../assets/images/warning.svg";

import { randomId } from "../../utilities/utils.js";
import "./toast.scss";

function Toast(props) {
  const toastElement = useRef();
  const { toastList, position } = props;
  const [list, setList] = useState(toastList);
  const [removeToast, setRemoveToast] = useState();

  const toastTypes = {
    success: {
      backgroundColor: "#34cf4d",
      icon: checkIcon,
    },
    danger: {
      backgroundColor: "#d93532",
      icon: errorIcon,
    },
    info: {
      backgroundColor: "#29a9ff",
      icon: infoIcon,
    },
    warning: {
      backgroundColor: "#ff8c19",
      icon: warningIcon,
    },
  };

  const deleteToast = useCallback(
    (id) => {
      setRemoveToast(id)
      setTimeout(() => {
        const index = list.findIndex((e) => e.id === id);
        list.splice(index, 1);
        setList([...list]);
      }, 500);
    },
    [list]
  );

  useEffect(() => {
    toastList.forEach((toast) => {
      toast.id = randomId();
    });
    setList(toastList);
  }, [toastList]);

  useEffect(() => {
    const currentToast = toastList[0];
    const interval = setInterval(() => {
      if (toastList.length && currentToast.autoDeleteTime && list.length) {
        deleteToast(currentToast.id);
      }
    }, currentToast?.autoDeleteTime);
    return () => {
      clearInterval(interval);
    };
  }, [deleteToast, list, toastList]);

  return (
    <div className={`notification-container ${position}`}>
      {list.map((toast, i) => (
        <div
          ref={toastElement}
          key={i}
          className={`notification toast ${removeToast === toast.id ? "remove-bottom-right" : position}`}
          id={toast.id}
          style={{ backgroundColor: toastTypes[toast.type].backgroundColor }}
        >
          <div className="notification_image">
            <img src={toastTypes[toast.type].icon} alt="" />
          </div>
          <div className="notification_content">
            <p className="notification_title">{toast.title}</p>
            <p className="notification_message">{toast.description}</p>
          </div>
          <button onClick={() => deleteToast(toast.id)}>X</button>
        </div>
      ))}
    </div>
  );
}

Toast.defaultProps = {
  position: "bottom-right", //top-right, bottom-right, top-left, bottom-left
};

Toast.propTypes = {
  toastList: PropTypes.array.isRequired,
  position: PropTypes.string,
  autoDelete: PropTypes.bool,
  autoDeleteTime: PropTypes.number,
  type: PropTypes.oneOf([
    "top-right",
    "bottom-right",
    "top-left",
    "bottom-left",
  ]),
};

export default memo(Toast);

//https://blog.logrocket.com/how-to-create-a-custom-toast-component-with-react/#:~:text=Create%20a%20function%20called%20showToast%20and%20pass%20a%20parameter%20called%20type%20.&text=Import%20the%20useState%20hook%20and,position%20selected%20by%20the%20user.
