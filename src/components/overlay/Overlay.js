import React, { memo } from "react";
import PropTypes from 'prop-types';

import "./overlay-style.scss"

function Overlay(props) {
    return(
        <div className={`overlay ${props.className}`} style={{display: props.isShow ? "flex" : "none"}}>
            {props.children}
        </div>
    )
}

Overlay.propTypes = {
    isShow: PropTypes.bool
}

export default memo(Overlay)