import React, { memo } from "react";

import "./page-header.scss";

import background from "../../assets/images/footer-bg.jpg";

function PageHeader(props) {
  return (
    <div
      className="page-header"
      style={{ backgroundImage: `url(${background})` }}
    >
      <h2>{props.children}</h2>
    </div>
  );
}

export default memo(PageHeader);
