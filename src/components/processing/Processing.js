import React, { memo } from "react";

import Overlay from "../overlay/Overlay";

import processingImg from '../../assets/images/processing.gif'

import './processing.scss'

function Processing(props) {
  return (
    <Overlay isShow={props.isShow} className={"overlay_content-center"}>
      <img src={processingImg} alt="processing" className="processing_img"/>
    </Overlay>
  );
}
export default memo(Processing);
