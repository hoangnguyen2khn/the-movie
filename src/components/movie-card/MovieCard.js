import React, { memo } from "react";
import { Link } from "react-router-dom";

import Button from "../button/Button";

import { apiConfig, category } from "../../utilities/ApiManage";

import "./movie-card.scss";

function MovieCard(props) {
  const { item } = props;
  const id = item.id || item.movie_id;
  const cate = item.type || category[props.category]

  let path = "/" + cate + "/" + id;
  if (cate === category.tv) {
    path = "/" + cate + "/" + id + "/1/1";
  }

  const background = apiConfig.w500Image(
    item.poster_path || item.backdrop_path
  );

  return (
    <Link to={path}>
      <div
        className="movie-card"
        style={{ backgroundImage: `url(${background})` }}
      >
        <Button>
          <i className="bx bx-play"></i>
        </Button>
      </div>
      <h3 className="movie-card-title">{item.title || item.name}</h3>
    </Link>
  );
}

export default memo(MovieCard);
