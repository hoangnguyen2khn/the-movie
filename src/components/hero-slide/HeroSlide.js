import React, { memo, useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import {useAuth} from '../../contexts/AuthContext.js'

import SwiperCore, { Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react/swiper-react.js";
import Button, { OutlineButton } from "../button/Button";
import Modal from "../modal/Modal";

import tmdbApi, {
  apiConfig,
  category,
  movieType
} from "../../utilities/ApiManage";

import "./heroSlide-style.scss";

function HeroSlide() {
  SwiperCore.use([Autoplay]);
  const { setIsProcessing } = useAuth()
  const [isShowModal, setIsShowModal] = useState(false);
  const [currentTrainer, setCurrentTrainer] = useState("");
  const [slideMovies, setSlideMovies] = useState([]);

  useEffect(() => {
    setIsProcessing(true)
    const getMoviesSlide = async () => {
      try {
        const res = await tmdbApi.getMoviesList(movieType.popular);
        setSlideMovies(res.results.slice(0, 4))
      } catch(e) {
        throw new Error(e);
      }
    };
    getMoviesSlide();
    setIsProcessing(false)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handelWatchTrainer = async (item) => {
    setIsProcessing(true)
    try {
        const res = await tmdbApi.getVideos(category.movie, item.id);
        setCurrentTrainer('https://www.youtube.com/embed/' + res.results[0].key)
      } catch(e) {
        throw new Error(e);
      }
    setIsShowModal(true);
    setIsProcessing(false)
  };

  const handelCloseModal = () => {
    setIsShowModal(false);
    setCurrentTrainer("");
  };

  return (
    <div className="hero_slide">
      <Swiper
        grabCursor={true}
        spaceBetween={0}
        slidesPerView={1}
        autoplay={{ delay: 10000 }}
        className="mySwiper"
      >
        {slideMovies.map((item, index) => {
          return (
            <SwiperSlide key={index}>
              {({ isActive }) => {
                return (
                  <HeroSlideItem
                    item={item}
                    className={`${isActive ? "active" : ""}`}
                    handelOnClick={() => handelWatchTrainer(item)}
                  />
                );
              }}
            </SwiperSlide>
          );
        })}
      </Swiper>
      <TrainerModal
        trainer_path={currentTrainer}
        isShowModal={isShowModal}
        handelOnClick={() => handelCloseModal()}
      />
    </div>
  );
}

function HeroSlideItem(props) {
  let navigate = useNavigate();

  const background = apiConfig.originalImage(props.item.backdrop_path ? props.item.backdrop_path : props.item.poster_path);
  return (
    <div
      className={`hero_slide_item ${props.className}`}
      style={{ backgroundImage: `url(${background})` }}
    >
      <div className="hero_slide_item_content">
        <div className="hero_slide_item_content_info">
          <h2 className="title">{props.item.title}</h2>
          <div className="overview">{props.item.overview}</div>
          <div className="btns">
            <Button onClick={() => navigate('/movie/' + props.item.id)}>Watch now</Button>
            <OutlineButton onClick={props.handelOnClick}>
              Watch trailer
            </OutlineButton>
          </div>
        </div>
        <div className="hero_slide_item_content_poster">
          <img src={apiConfig.w500Image(props.item.poster_path)} alt="background" />
        </div>
      </div>
    </div>
  );
}

function TrainerModal(props) {
  return (
    <Modal isShow={props.isShowModal} handelOnClick={props.handelOnClick}>
      <iframe
        src={props.trainer_path}
        width="100%"
        height="500px"
        title="trailer"
      ></iframe>
    </Modal>
  );
}

export default memo(HeroSlide);
