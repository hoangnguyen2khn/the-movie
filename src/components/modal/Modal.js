import React, { memo } from "react";
import PropTypes from 'prop-types';

import Overlay from "../overlay/Overlay";
import Button from "../button/Button";

import "./modal-style.scss";

function Modal(props) {
  return (
    <Overlay isShow={props.isShow} className={"overlay_content-center"}>
      <div className={`modal ${props.isShow ? "active" : ""}`}>
        <Button className={"btn_close"} onClick={props.handelOnClick}>
          X
        </Button>
        {props.children}
      </div>
    </Overlay>
  );
}

Modal.propTypes = {
    isShow: PropTypes.bool
}

export default memo(Modal);
