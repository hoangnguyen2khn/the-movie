import React, { memo, useState, useEffect, useCallback } from "react";
import { useAuth } from "../../contexts/AuthContext.js";

import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";

import MovieCard from "../movie-card/MovieCard";
import Button, { OutlineButton } from "../button/Button";
import Input from "../input/Input";

import tmdbApi, {
  category,
  movieType,
  tvType,
} from "../../utilities/ApiManage";

import "./movie-grid.scss";

function MovieGrid(props) {
  const navigate = useNavigate();
  const { setIsProcessing, getListFavoriteMovies, currentUser } = useAuth();
  const { keyword } = useParams();

  const [movies, setMovies] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);

  useEffect(() => {
    setIsProcessing(true);
    if(props.category === category.favorites && !currentUser) return navigate(`/`);;
    const getList = async () => {
      let res = null;
      if (keyword === undefined) {
        switch (props.category) {
          case category.movie:
            res = await tmdbApi.getMoviesList(movieType.upcoming);
            break;
          case category.favorites:
            res = await getListFavoriteMovies(currentUser.uid);
            break;
          default:
            res = await tmdbApi.getTvList(tvType.popular);
        }
      } else {
        const params = {
          query: keyword,
        };
        res = await tmdbApi.search(props.category, params);
      }
      let result;
      if (res.results) {
        result = res.results;
      } else {
        result = res;
      }
      setMovies(result);
      setTotalPage(res.total_pages);
      setIsProcessing(false);
    };
    getList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.category, keyword, currentUser]);

  const loadMore = async () => {
    let res = null;
    if (keyword === undefined) {
      const params = {
        page: page + 1,
      };
      switch (props.category) {
        case category.movie:
          res = await tmdbApi.getMoviesList(movieType.upcoming, {
            params,
          });
          break;
        default:
          res = await tmdbApi.getTvList(tvType.popular, { params });
      }
    } else {
      const params = {
        page: page + 1,
        query: keyword,
      };
      res = await tmdbApi.search(props.category, { params });
    }
    setMovies([...movies, ...res.results]);
    setPage(page + 1);
  };

  return (
    <>
      {props.category !== category.favorites && (
        <div className="section mb-3 section-mobile">
          <MovieSearch category={props.category} keyword={keyword} />
        </div>
      )}
      <div className="movie-grid">
        {movies.map((item, i) => (
          <MovieCard category={props.category} item={item} key={i} />
        ))}
      </div>
      {page < totalPage ? (
        <div className="movie-grid_loadmore">
          <OutlineButton className="small" onClick={loadMore}>
            Load more
          </OutlineButton>
        </div>
      ) : null}
    </>
  );
}

const MovieSearch = (props) => {
  const navigate = useNavigate();

  const [keyword, setKeyword] = useState(props.keyword ? props.keyword : "");

  const goToSearch = useCallback(() => {
    if (keyword.trim().length > 0) {
      navigate(`/${category[props.category]}/search/${keyword}`);
    } else {
      navigate(`/${category[props.category]}`);
    }
  }, [keyword, props.category, navigate]);

  useEffect(() => {
    const enterEvent = (e) => {
      e.preventDefault();
      if (e.keyCode === 13) {
        goToSearch();
      }
    };
    document.addEventListener("keyup", enterEvent);
    return () => {
      document.removeEventListener("keyup", enterEvent);
    };
  }, [keyword, goToSearch]);

  return (
    <div className="movie-search">
      <Input
        type="text"
        placeholder="Enter keyword"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
      />
      <Button className="small" onClick={goToSearch}>
        Search
      </Button>
    </div>
  );
};

export default memo(MovieGrid);
