import { useEffect, useState, useRef, memo } from "react";
import { Link, useLocation } from "react-router-dom";
import { useAuth } from "../../contexts/AuthContext.js";

import Logo from "../logo/Logo";
import userDefaultImg from "../../assets/images/user_default.jpg";

import "./header-style.scss";

function Header() {
  const { currentUser, signInWithGoogle, signOutInfo } = useAuth();
  const { pathname } = useLocation();
  const headerRef = useRef();

  const [isOpenUserMenu, setIsOpenUserMenu] = useState(false);

  const headerNav = [
    {
      display: "Home",
      path: "/",
    },
    {
      display: "Movies",
      path: "/movie",
    },
    {
      display: "TV Series",
      path: "/tv",
    },
    {
      display: "Login",
      type: "auth",
    },
  ];

  const userMenu = [
    {
      display: "Favorites",
      path: "/favorites",
    },
    {
      display: "Logout",
      handel: signOutInfo,
    },
  ];

  const active = headerNav.findIndex((e) => e.path === pathname);

  useEffect(() => {
    const shrinkHeader = () => {
      if (
        document.body.scrollTop > 10 ||
        document.documentElement.scrollTop > 10
      ) {
        headerRef.current.classList.add("shrink");
      } else {
        headerRef.current.classList.remove("shrink");
      }
    };
    window.addEventListener("scroll", shrinkHeader);
    return () => {
      window.removeEventListener("scroll", shrinkHeader);
    };
  }, []);

  return (
    <header className="header" ref={headerRef}>
      <Logo path={"/"} />
      <ul className="header_nav">
        {headerNav.map((item, index) => {
          if (!currentUser && item.type === "auth") {
            return (
              <li
                key={index}
                onClick={signInWithGoogle}
                className={"login_btn"}
              >
                {item.display}
              </li>
            );
          }
          if (currentUser && item.type === "auth") {
            return (
              <li
                key={index}
                onClick={() => setIsOpenUserMenu(!isOpenUserMenu)}
              >
                <img
                  src={currentUser.photoURL || userDefaultImg}
                  className={"user_image"}
                  alt="User_image"
                />
                {isOpenUserMenu && <UserMenu menu={userMenu} />}
              </li>
            );
          }
          return (
            <li key={index} className={`${index === active ? "active" : ""}`}>
              <Link to={item.path}>{item.display}</Link>
            </li>
          );
        })}
      </ul>
    </header>
  );
}

function UserMenu(props) {
  const { menu } = props;

  return (
    <div className={"user_menu"}>
      <ul>
        {menu.map((item, index) => {
          if (item.path) {
            return (
              <Link to={item.path} key={index}>
                <li onClick={item?.handel}>{item.display}</li>
              </Link>
            );
          }
          return <li onClick={item?.handel} key={index}>{item.display}</li>;
        })}
      </ul>
    </div>
  );
}

export default memo(Header);
