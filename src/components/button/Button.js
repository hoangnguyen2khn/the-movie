import React, { memo } from "react";
import PropTypes from 'prop-types';

import './button-style.scss';

function Button(props) {
  return (
    <button
      className={`btn ${props.className && props.className}`}
      onClick={props.onClick ? () => props.onClick() : null}
    >
      {props.children}
    </button>
  );
}

export function OutlineButton(props) {
  return (
    <Button
      className={`btn-outline ${props.className && props.className}`}
      onClick={props.onClick ? () => props.onClick() : null}
    >
      {props.children}
    </Button>
  );
}

Button.propTypes = {
    onClick: PropTypes.func
}

export default memo(Button);
