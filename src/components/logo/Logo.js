import { memo } from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/images/logo.png";
import "./logo-style.scss"

function Logo(props) {
  return (
    <Link to={props.path} className="logo">
      <img src={logo} className="logo_image" alt="logo" />
      <p className="logo_text">movie</p>
    </Link>
  );
}

export default memo(Logo);
