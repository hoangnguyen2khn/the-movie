import React, { memo, useState, useEffect } from "react";
import { useAuth } from "../../contexts/AuthContext.js";
import PropTypes from "prop-types";

import { Swiper, SwiperSlide } from "swiper/react/swiper-react.js";
import MovieCard from "../movie-card/MovieCard";

import tmdbApi, { category } from "../../utilities/ApiManage";

import "./movie-list.scss";

function MovieList(props) {
  const { setIsProcessing } = useAuth();
  const [movieArr, setMovieArr] = useState([]);

  useEffect(() => {
    setIsProcessing(true);
    const getMoviesListTypes = async () => {
      try {
        let res = null;
        if (props.type !== "similar") {
          switch (props.category) {
            case category.movie:
              res = await tmdbApi.getMoviesList(props.type);
              break;
            default:
              res = await tmdbApi.getTvList(props.type);
          }
        } else {
          res = await tmdbApi.similar(props.category, props.id);
        }
        setMovieArr(res.results);
      } catch (e) {
        throw new Error(e);
      }
      setIsProcessing(false);
    };
    getMoviesListTypes();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  return (
    <div className="movie_list">
      <Swiper grabCursor={true} spaceBetween={10} slidesPerView={"auto"}>
        {movieArr.map((item, index) => {
          return (
            <SwiperSlide key={index}>
              <MovieCard item={item} category={props.category} />
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  );
}

MovieList.propTypes = {
  category: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default memo(MovieList);
