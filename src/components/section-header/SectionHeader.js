import React, { memo } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { OutlineButton } from "../../components/button/Button";

function SectionHeader(props) {
  return (
    <div className="section mb-3">
      <div className="section_header mb-2">
        <h2>{props.title}</h2>
        {props.path && (
          <Link to={props.path}>
            <OutlineButton className="small">{props.btnTitle}</OutlineButton>
          </Link>
        )}
      </div>
      {props.children}
    </div>
  );
}

SectionHeader.propTypes = {
  title: PropTypes.string,
  path: PropTypes.string,
};

export default memo(SectionHeader);
