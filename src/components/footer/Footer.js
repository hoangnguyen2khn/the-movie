import { memo } from "react";
import { Link } from "react-router-dom";
import Logo from "../logo/Logo";

import "./footer-style.scss";

const ListItem = [
  [
    {
      title: "Home",
      path: "/",
    },
    {
      title: "Contact us",
      path: "/",
    },
    {
      title: "Term of services",
      path: "/",
    },
    {
      title: "About us",
      path: "/",
    },
  ],
  [
    {
      title: "Live",
      path: "/",
    },
    {
      title: "FAQ",
      path: "/",
    },
    {
      title: "Premium",
      path: "/",
    },
    {
      title: "Pravacy policy",
      path: "/",
    },
  ],
  [
    {
      title: "You must watch",
      path: "/",
    },
    {
      title: "Recent release",
      path: "/",
    },
    {
      title: "Top IMDB",
      path: "/",
    },
  ],
];

function Footer(params) {
  return (
    <div className="footer">
      <div className="footer_content container">
        <div className="footer_content_logo">
          <Logo path={"/"} />
        </div>
        <div className="footer_content_menus">
          {ListItem.map((item, index) => {
            return (
              <div className="footer_content_menu" key={index}>
                {item.map((subItem, subIndex) => {
                    return(
                        <Link to={subItem.path} key={subIndex}>{subItem.title}</Link>
                    )
                })}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default memo(Footer);
