import React, { memo, useEffect } from "react";

import { useParams } from "react-router";

import PageHeader from "../../components/page-header/PageHeader";
import MovieGrid from "../../components/movie-grid/MovieGrid";

import { category as cate } from "../../utilities/ApiManage";
import params, { capitalizeFirstLetter } from "../../utilities/utils";

function Catalog() {
  const { category } = useParams();
  useEffect(() => {
    window.scrollTo(0, 0);
    document.title = capitalizeFirstLetter(category) + " | " + params.webTitle;
  }, [category]);

  return (
    <>
      <PageHeader>
        {category === cate.movie ? "Movies" : (category === cate.tv ? "TV Series" : "Favorites")}
      </PageHeader>
      <div className="container">
        <div className="section mb-3">
          <MovieGrid category={category} />
        </div>
      </div>
    </>
  );
}

export default memo(Catalog);
