import React, { memo, useEffect, useState } from "react";
import { useAuth } from "../../contexts/AuthContext.js";
import { useParams } from "react-router";
import { Link } from "react-router-dom";

import MovieList from "../../components/movie-list/MovieList";
import SectionHeader from "../../components/section-header/SectionHeader";
import { OutlineButton } from "../../components/button/Button";

import tmdbApi, {
  apiConfig,
  category as cate,
} from "../../utilities/ApiManage";
import params from "../../utilities/utils";

import "./detail-style.scss";

function Detail(props) {
  const { setIsProcessing, getFavoriteMovie, toggleFavoriteMovie, setToast } =
    useAuth();
  const { category, id, episode, season } = useParams();
  const [urlTvMovie, setUrlTvMovie] = useState("");
  const [movie, setMovie] = useState();
  const [isFavorite, setIsFavorite] = useState(false);

  useEffect(() => {
    setIsProcessing(true);
    if (category === cate.tv) {
      setUrlTvMovie(
        `${apiConfig.movieFrame}/${category}?id=${id}&s=${season}&e=${episode}`
      );
    } else {
      setUrlTvMovie(`${apiConfig.movieFrame}/${category}?id=${id}`);
    }
    const getDetail = async () => {
      const res = await tmdbApi.detail(category, id, { params: {} });
      setMovie(res);
      window.scrollTo(0, 0);
      document.title =
        (res.title ? res.title : res.name) + (" | " + params.webTitle);
      setIsProcessing(false);
    };
    getDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [category, id, episode, season]);

  useEffect(() => {
    getFavoriteMovie(id).then((data) => {
      // eslint-disable-next-line eqeqeq
      if (id == data?.movie_id) {
        setIsFavorite(true);
        return;
      }
      setIsFavorite(false);
    });
  }, [getFavoriteMovie, id]);

  useEffect(() => {
    setToast({
      type: "info",
      title: "Tips",
      description: "Change server if the movie is slow to load",
      autoDeleteTime: 5000,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handelToggleFavoriteMovie = () => {
    toggleFavoriteMovie(movie);
  };

  return (
    <>
      {movie && (
        <>
          <div
            className="banner"
            style={{
              backgroundImage: `url(${apiConfig.originalImage(
                movie.backdrop_path || movie.poster_path
              )})`,
            }}
          ></div>
          <div className="mb-3 movie_content container">
            <div className="movie_content_poster">
              <div
                className="movie_content_poster_img"
                style={{
                  backgroundImage: `url(${apiConfig.originalImage(
                    movie.poster_path || movie.backdrop_path
                  )})`,
                }}
              ></div>
            </div>
            <div className="movie_content_info">
              <h1 className="title">{movie.title || movie.name}</h1>
              <div className="genres">
                <span
                  className="genres_item genres_item_favotite"
                  onClick={handelToggleFavoriteMovie}
                >
                  {isFavorite ? "❤ Favorited" : "Add to favorites"}
                </span>
                {movie.genres &&
                  movie.genres.slice(0, 5).map((genre, i) => (
                    <span key={i} className="genres_item">
                      {genre.name}
                    </span>
                  ))}
              </div>
              <p className="overview">{movie.overview}</p>
              <div className="cast">
                <div className="section_header">
                  <h2>Casts</h2>
                </div>
                <CastList id={movie.id} />
              </div>
            </div>
          </div>
          <div className="container">
            <div className="video section mb-3">
              <iframe
                src={urlTvMovie}
                width="70%"
                title="Movie player"
                frameBorder="0"
                allowFullScreen="allowFullScreen"
              ></iframe>
            </div>
            {category === cate.tv && (
              <ListSeason
                movie={movie}
                id={id}
                category={category}
                season={season}
                episode={episode}
              />
            )}
            <SectionHeader title={"Similar"}>
              <MovieList category={category} type="similar" id={movie.id} />
            </SectionHeader>
          </div>
        </>
      )}
    </>
  );
}

function CastList(props) {
  const { category } = useParams();
  const [casts, setCasts] = useState([]);

  useEffect(() => {
    const getCredits = async () => {
      const res = await tmdbApi.credits(category, props.id);
      setCasts(res.cast.slice(0, 5));
    };
    getCredits();
  }, [category, props.id]);

  return (
    <div className="casts">
      {casts.map((item, i) => (
        <div key={i} className="casts_item">
          <div
            className="casts_item_img"
            style={{
              backgroundImage: `url(${apiConfig.w500Image(item.profile_path)})`,
            }}
          ></div>
          <p className="casts_item_name">{item.name}</p>
        </div>
      ))}
    </div>
  );
}

function ListSeason(props) {
  const { movie, category, id, season, episode } = props;

  return (
    <SectionHeader title={"Season - Episode"}>
      {movie.seasons.map((item, index) => {
        let episodes = [];
        for (let i = 1; i <= item.episode_count; i++) {
          episodes.push(i);
        }
        return (
          <div className="season_item" key={index}>
            <h4 className="season_item_title">{item.name}</h4>
            <div className="episodes">
              {episodes.map((eItem, eIndex) => {
                let isActive = false;
                const isDisabled =
                  movie.last_episode_to_air.episode_number <= eIndex &&
                  movie.last_episode_to_air.season_number === index + 1;
                if (
                  season === item.season_number.toString() &&
                  episode === eItem.toString()
                ) {
                  isActive = true;
                }
                if (isDisabled) {
                  return (
                    <OutlineButton
                      className={`episode small ${isActive && "active"} ${
                        isDisabled && "disabled"
                      }`}
                      key={eIndex}
                    >
                      {eItem}
                    </OutlineButton>
                  );
                }
                return (
                  <Link
                    to={`/${category}/${id}/${item.season_number}/${eItem}`}
                    key={eIndex}
                  >
                    <OutlineButton
                      className={`episode small ${isActive && "active"}`}
                    >
                      {eItem}
                    </OutlineButton>
                  </Link>
                );
              })}
            </div>
          </div>
        );
      })}
    </SectionHeader>
  );
}

export default memo(Detail);
