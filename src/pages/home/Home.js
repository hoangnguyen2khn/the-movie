import React, { memo, useEffect } from "react";
import HeroSlide from "../../components/hero-slide/HeroSlide";
import MovieList from "../../components/movie-list/MovieList";
import SectionHeader from "../../components/section-header/SectionHeader";

import { category, movieType, tvType } from "../../utilities/ApiManage";
import params from "../../utilities/utils";

import "./home-style.scss";

function Home() {
  useEffect(() => {
    document.title = params.webTitle;
  }, []);

  const MovieArr = [
    {
      title: "Trending Movies",
      path: "/movie",
      category: category.movie,
      type: movieType.popular,
    },
    {
      title: "Top Rated Movies",
      path: "/movie",
      category: category.movie,
      type: movieType.top_rated,
    },
    {
      title: "Trending TV",
      path: "/tv",
      category: category.tv,
      type: tvType.popular,
    },
    {
      title: "Top Rated TV",
      path: "/tv",
      category: category.tv,
      type: tvType.top_rated,
    },
  ];
  return (
    <>
      <HeroSlide />
      <div className="container">
        {MovieArr.map((item, index) => {
          return (
            <SectionHeader
              title={item.title}
              path={item.path}
              btnTitle={"View more"}
              key={index}
            >
              <MovieList category={item.category} type={item.type} />
            </SectionHeader>
          );
        })}
      </div>
    </>
  );
}

export default memo(Home);
