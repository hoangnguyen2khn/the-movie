import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import "./App.scss";

import React from "react";
import {useAuth} from "./contexts/AuthContext.js";
import { BrowserRouter as Router } from "react-router-dom";

import Header from "../src/components/header/Header";
import Footer from "../src/components/footer/Footer";
import Processing from "./components/processing/Processing";
import Toast from "./components/toast/Toast";

import RoutesConfig from "./config/Routes";

function App() {
  const { isProcessing, toastList } = useAuth();

  return (
    <Router>
      <>
        <Header />
        <RoutesConfig />
        <Footer />
        <Processing isShow={isProcessing} />
        <Toast toastList={toastList} position="bottom-right"/>
      </>
    </Router>
  );
}

export default App;
