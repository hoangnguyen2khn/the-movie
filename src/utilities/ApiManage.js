/**
 * Created by SongHoang on 16/10/21.
 */

import { getWithTimeout } from "./networking";

export const apiConfig = {
  baseUrl: "https://api.themoviedb.org/3",
  apiKey: "2199f9775718734b07129fef98b449e2",
  movieFrame: "https://www.2embed.ru/embed/tmdb",
  originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
  w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`,
};

export const category = {
  movie: "movie",
  tv: "tv",
  favorites: "favorites"
};

export const movieType = {
  upcoming: "upcoming",
  popular: "popular",
  top_rated: "top_rated",
};

export const tvType = {
  popular: "popular",
  top_rated: "top_rated",
  on_the_air: "on_the_air",
};

const getParams = (params) => {
  let keys;
  let values;
  let result = "";
  if (!params.params) {
    keys = Object.keys(params);
    values = Object.values(params);
    result = `&${keys}=${values}`;
  } else {
    keys = Object.keys(params.params);
    values = Object.values(params.params);
    for (let i = 0; i < keys.length; i++) {
      result += "&" + keys[i] + "=" + values[i];
    }
  }
  return result;
};

const urlApiKey = (url, params) => {
  const apiKey = `api_key=${apiConfig.apiKey}`;
  let result = `${apiConfig.baseUrl}/${url}?${apiKey}`;
  if (!params) {
    return result;
  }
  return result + getParams(params);
};

const tmdbApi = {
  getMoviesList: (type, params) => {
    const url = "movie/" + movieType[type];
    return getWithTimeout(urlApiKey(url, params), {});
  },
  getTvList: (type, params) => {
    const url = "tv/" + tvType[type];
    return getWithTimeout(urlApiKey(url, params), {});
  },
  getVideos: (cate, id) => {
    const url = category[cate] + "/" + id + "/videos";
    return getWithTimeout(urlApiKey(url), {});
  },
  search: (cate, params) => {
    const url = "search/" + category[cate];
    return getWithTimeout(urlApiKey(url, params), {});
  },
  detail: (cate, id, params) => {
    const url = category[cate] + "/" + id;
    return getWithTimeout(urlApiKey(url, params), {});
  },
  credits: (cate, id) => {
    const url = category[cate] + "/" + id + "/credits";
    return getWithTimeout(urlApiKey(url), {});
  },
  similar: (cate, id) => {
    const url = category[cate] + "/" + id + "/similar";
    return getWithTimeout(urlApiKey(url), {});
  },
};
export default tmdbApi;
