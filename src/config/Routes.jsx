import React from 'react';

import { Routes , Route } from 'react-router-dom';

import Home from '../pages/home/Home.js';
import Catagory from '../pages/category/Catagory.js';
import Detail from '../pages/detail/Detail.js';

const RoutesConfig = () => {
    return (
        <Routes>
            <Route
                path='/:category/search/:keyword'
                element={<Catagory />}
            />
            <Route
                path='/:category/:id'
                element={<Detail />}
            />
            <Route
                path='/:category/:id/:season/:episode'
                element={<Detail />}
            />
            <Route
                path='/:category'
                element={<Catagory />}
            />
            <Route
                path='/'
                exact
                element={<Home/>}
            />
        </Routes>
    );
}

export default RoutesConfig;
