# react-movie

    Responsive React Movies App With API

# Resource

    Google font: https://fonts.google.com/

    Boxicons: https://boxicons.com/

    Images: https://unsplash.com/

    API: https://www.themoviedb.org/

    Theme: https://www.2embed.ru/

# Setup
    npm i

# Build
    npm start